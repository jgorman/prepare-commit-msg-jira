# JIRA prepare-commit-msg hook

This [prepare-commit-msg](https://www.kernel.org/pub/software/scm/git/docs/githooks.html#_prepare_commit_msg) hook will automatically prepend the JIRA issue key prefix from the currently checked out branch to new commit messages. 

Your branch must start with an issue key (e.g. `STASH-1234-fix-a-bug`), or have an issue key after a prefix path (e.g. `bugfix/STASH-1234-fix-a-bug`), for this to work. If your current branch _does not_ start with an issue key, the hook will leave the default commit message intact.

## Installation

Run the following:
    
    curl -so ${bin}/jira-prepare-commit-msg https://bitbucket.org/tpettersen/prepare-commit-msg-jira/raw/master/jira-prepare-commit-msg
    curl -so ${bin}/clone https://bitbucket.org/tpettersen/prepare-commit-msg-jira/raw/master/clone
    chmod a+x ${bin}/jira-prepare-commit-msg ${bin}/clone

for some path `${bin}` in your `$PATH`, eg `${HOME}/bin`. 

You can now use `clone` to replace `git clone` - this will clone the repository, then symlink the `prepare-commit-msg` into the repositories hooks directory.

To add this to an existing repository you can sym-link or copy `${bin}/jira-prepare-commit-msg` to `.git/hooks/prepare-commit-msg` in the repo, eg using

    ln -s `which jira-prepare-commit-msg` .git/hooks/prepare-commit-msg

## Overload git in BASH

You can add the following to your `.bash_profile` to replace git's clone behaviour:

    git() {
      if [[ $1 == "clone" ]]; then
        shift
        command clone "$@"
      else
        command git "$@"
      fi 
    }

## Using the hook with Git for Windows

If you're using Git on Windows you'll have to replace grep with a more up-to-date one that supports the "-o"/"--onlymatching" switch, for example GNU Grep for Windows:
http://gnuwin32.sourceforge.net/packages/grep.htm

Download including the dependencies package and place under [GitInstallation]\bin overwriting the original grep.exe

Additionally you need MkTemp which is not part of Git for Windows:
http://gnuwin32.sourceforge.net/packages/mktemp.htm

Download and place under [GitInstallation]\bin
